### CFP Pricing Documentation

*Note : Pricing is for FR4 material only*



## Sheet 1 [Section 1]

1. Get board width (BW) and board height (BH) and Quantity (QTY) and layers (LYR) from user.
2. Calculate Board Size (BS) = BW x BH.
3. Calculate total Sq. Inch (TOT_SQ_INCH) = BS x QTY
4. Convert TOT_SQ_INCH to Square meter (TOT_SQ_MTR)
5. Look at Sheet 1 - Section 1 and get base price based on layers and TOT_SQ_MTR (Divide by 1550.003)
6. Look at Sheet 1 - Section 1 and get Standard Delivery time (STD_DELIVERY_TIME) 
7. Tooling and testing - convert single board size from sq. in to sq. cm and if value is less than equal to 600 then apply charge as per chart, if more than 600 add cost accordingly for every 200 sq.cm increase.
8. What is MOV for repeat customers **[DOUBT]**



## Sheet 1 [Section 2]

1. **Based Copper**
   Copper thickness is = inner/outer layer is 18/35um. 10% for every additional 1OZ (Maximum to 50OZ)
   **[DOUBT]** : current form has Inner/outer cu and total number of layers having 2/3/4 OZ copper
2. **over one type board in one panel**
   are they saying if there is multiple board design and every additional board design is charged at $40 extra for tooling?
3. **single board area under 25sq cm and without tooling hole inside the board**
   which field is this related on speedy form? also the multiplier do we total and then apply or multiply as we go?
4. **Gold finger**
   what is the conversion of u" to um of speedy (Speedy has 30u" and 50u" while CFP has 0.25um to 0.75um). is 0.25um = 25u"?
5. **Immersion Gold**
   Mapped to Finish Type of Speedy
6. **Immersion Tin**
   Mapped to Finish Type of Speedy
7. **Immersion Sliver**
   Mapped to Finish Type of Speedy
8. **Peelable solder mask/carbon printing**
   **[DOUBT]** : What is the mapping to Speedy?
9. **blind or buried vias**
   **[DOUBT]** : Speedy has Laser or mechanical, also speedy has sets (1SET, 2 SET) while CFP has laminating 1/2/3 times, how to combine and show in single form?
10. **HDI(refer to use lase drilling,the min drilling hole <=0.15,min track<3MIL)**
    **[DOUBT]** : What is the mapping to Speedy?
11. **high TG>=1700(S1170) & high TG>=1800(IT180)**
    **[DOUBT]** : What is the mapping to Speedy?
12. **Halogen Free (Normal/High Tg)** 
    Is this Finish type?
13. **min copper thickness on the hole wall**
    **[DOUBT]** : What is the mapping to Speedy?
14. **Inspection standard** 
    Same as IPC class of Speedy
15. **Hole density（more than 100K/m2)**
    Same as speedy
16. **Min hole size(mechanical)**
    Same as Speedy, has smaller set of values than speedy, eliminate accordingly
17. **Min trace width/spacing in outer layer (before fab compensation)**
    Same as speedy
18. **Fake layer stack up(for example 4-layer board using 2 or more core per stack up)**
    Non existent in other vendors, need to add to form, please explain (NRE remains the same as the real circuit layer count, the unit cost would be the average value of the real layer count and the fake layer count.)
19. **High aspect ratio(12:1 or above)**
    Is this similar to aspect ratio in speedy
20. **High precision impedance tolerance**
    Speedy has sets and CFP has %, how to set a common ground?
21. **Warpage (not including blind&buried via board)**
    Seem like does not exist in the current form, need to be added
22. **Printing the specify serial number in the silkscreen layer**
    Similar to Mask sides in the existing form



## Sheet 1 [Section 3] [Board Thickness]

- Each layer has a standard thickness (eg. 2 Layers has <= 2mm). 2.1mm to 3mm has 10% multiplier. 3.1 to 4mm has 30% multiplier and 5.1 to 6mm has 100% multiplier. What about above 6mm? is it not applicable?



## Sheet 1 [Section 4] [Day based adjustments]

- Batch area under 1sq m, has only 2 and 4 layers, what if there's 6 layers and batch area less than 1sq m
- additional cost calculation is similar to Speedy, check standard delivery from section 1 then calculate for each day earlier based on percentage given in the chart
- what is MOQ >= 2 Panel at the bottom?



## Sheet 2 [Hard Gold Sheet]

- gold thickness can be in either micro meter or micro inch, then further calculations are based on area less than 1sq m (price per order in this case) or area greater than 1 sq m (price per sq meter in this case). 
- sheet says **If the order size under 1sq m, Fastpirnt will charge the MOV. The order size over 1sq m, the extra cost will be charged.**
  Can you explain this further? 
- also does speedy (existing form have) Hard Gold? I see Selective Hard Gold which is supported by speedy as seen in the PCB quote form.



## Sheet 3 [Selective Hard Gold]

- **The selective gold plating area = 25% single boards area.**
  please eloborate
- sheet says **If the order size under 1sq m, Fastpirnt will charge the MOV. The order size over 1sq m, the extra cost will be charged.**
  Can you explain this further? 



## Sheet 4 [RF 2-Layer]

- 

